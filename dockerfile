FROM nginx:1.21
RUN mkdir /app
WORKDIR /app
COPY ./website /app/public
COPY ./default.conf /etc/nginx/conf.d/default.conf
